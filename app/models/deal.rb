class Deal < ActiveRecord::Base
  has_many :branches_deals
  belongs_to :business
  has_many :images, as: :resource
  belongs_to :sub_category, class_name: 'Category'
  has_many :branches, through: :branches_deals

  validates :name,:description, :presence => true
  validates :price,:deal_price,:presence => true, numericality: {only_float: true}
  validate :validate_deal

  default_scope { where(deleted: false) }

  def validate_deal
    errors.add(:branches, "A business should have at least one or more branches") if self.business.branches.count == 0
    if app_post
      errors.add(:terms_and_conditions, "Please enter Terms and Conditions before publishing the app") if terms_and_conditions.blank?
      errors.add(:redeem_steps, "Please enter Steps to redeem before publishing the app") if terms_and_conditions.blank?
    end
  end

  def destroy
    self.deleted = true
    self.deleted_at = Time.now
    self.save
  end
end
