class User < ActiveRecord::Base
  has_many :branch_views
  has_many :recently_viewed_branches,-> { distinct }, through: :branch_views, source: :branch

  has_many :branch_favorites
  has_many :favorited_branches,-> { distinct }, through: :branch_favorites, source: :branch

end
