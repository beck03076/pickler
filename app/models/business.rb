class Business < ActiveRecord::Base
  has_many :branches, dependent: :destroy
  has_many :deals, dependent: :destroy
  has_many :images, as: :resource
  belongs_to :category

  validates :name,:logo,:category_id, :presence => true

  default_scope { where(deleted: false) }

  def destroy
    self.deleted = true
    self.deleted_at = Time.now
    self.save
  end

end
