class Location < ActiveRecord::Base
	has_many :branches
	has_many :deals, through: :branches
end
