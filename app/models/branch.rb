class Branch < ActiveRecord::Base
#  searchkick locations: ["location"]

  has_many :ratings, dependent: :destroy
  has_many :branches_deals
  belongs_to :business


  validates_numericality_of :close_time, :less_than_or_equal_to => 86400
  validates_numericality_of :open_time, :less_than_or_equal_to => 86400
  validates :business_id, :presence => true
  validate :validate_branch


  default_scope { where(deleted: false) }

  def validate_branch
    if open_time > close_time
      errors.add(:open_time, "can't be greater than close time")
    end
    if landline.blank? & mobile.blank?
      errors.add(:contact, "Please enter either mobile or landline")
    end
  end

  def should_index?
    latitude.present? && longitude.present?
  end

  def search_data
    {
      location: [latitude, longitude]
    }
  end

  def destroy
    self.deleted = true
    self.deleted_at = Time.now
    self.save
  end

end
