class Category < ActiveRecord::Base
	has_many :businesses
	has_many :deals
  has_many :subcategories, class_name: "Category", foreign_key: "parent_id"

  default_scope { where(deleted: false) }

  def destroy
    self.deleted = true
    self.deleted_at = Time.now
    self.save
  end

end
