class DealSerializer < ActiveModel::Serializer
  attributes :id,:name,:business,:price,:deal_price,:description,:available_days,:start_time,:end_time,:start_date,:end_date,:deal_type,:original_qty,:current_qty,:limited,:all_branches,:bought_count,:redeemed_count,:app_buyable,:cash_buyable
  has_one :business
  has_many :branches_deals
  has_many :images
  def available_days
    available_days_array = []
    if object.available_days
      available_days_stat = object.available_days.split(',')
      days_of_week = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']
      days_of_week.each_with_index do |value, index|
        if available_days_stat[index].to_i==1
          available_days_array.push(value)
        end
      end
    end
    available_days_array
  end
end
