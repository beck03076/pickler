class BusinessSerializer < ActiveModel::Serializer
  attributes :id,:name,:category
  has_one :category , serializer: SubCategorySerializer
end
