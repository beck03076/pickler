class BranchSerializer < ActiveModel::Serializer
  attributes :id,:name,:branch_area,:category,:location_id,:open_time,:close_time,:keywords,:rating,:working_days
  has_one :business
  def category
    object.business.try(:category).try(:name)
  end
  def keywords
    object.business.try(:keywords)
  end
  def rating
   ratings = 0
   if object.ratings.count > 0
    ratings = ((object.ratings.sum :rating) / object.ratings.count).round(2)
   end
   ratings
  end
  def working_days
    available_days_array = []
    if object.working_days
      available_days_stat = object.working_days.split(',')
      days_of_week = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']
      days_of_week.each_with_index do |value, index|
        if available_days_stat[index].to_i==1
          available_days_array.push(value)
        end
      end
    end
    available_days_array
  end
end
