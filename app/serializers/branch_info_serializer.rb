class BranchInfoSerializer < ActiveModel::Serializer
  attributes :id,:description,:address,:location_id,:website,:facebook,:twitter,:contacts,:no_of_people_who_rated,:images,:state,:logo,:highlights,:additional_info
  has_many :images
  def description
  	object.business.description
  end
  def website
  	object.business.website
  end
  def facebook
  	object.business.facebook
  end
  def twitter
  	object.business.twitter
  end
  def contacts
  	{ "mobile" => object.mobile, "landline" => object.landline }
  end
  def no_of_people_who_rated
    object.ratings.count
  end
  def images
    object.business.images
  end
  def logo
    object.business.logo
  end
end
