class BranchesDealSerializer < ActiveModel::Serializer
  attributes :branch,:original_qty,:current_qty
  has_one :branch, serializer: ::BranchesDealsInfoSerializer 
end
