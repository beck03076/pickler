class UserSerializer < ActiveModel::Serializer
  attributes :id
  has_many :branch_views
  has_many :branches, through: :branch_views
end
