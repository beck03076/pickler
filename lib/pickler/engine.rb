module Pickler
  class Engine < ::Rails::Engine
    isolate_namespace Pickler
  end
end
